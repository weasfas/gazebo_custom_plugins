# README #

Gazebo Wifi plugin README.

### Description ###

* Gazebo Plugin to implement a Wifi device. It will allow to simulate communication delay and problems realted with latency.
* This Plugin is based on a Gazebo Github plugin you can find [here](https://github.com/Lakshadeep/gazebo_wifi_plugin).

### Set up ###

1. Clone this repository into your catking workspace src folder:
> git clone git@bitbucket.org:weasfas/gazebo_wifi_plugin.git
2. Compile from src:
> catkin_make
3. Launch Gazebo with the Plugin with the all ready made World:
> roslaunch gazebo_wifi_plugins wifi_plugin.launch

### TODO List ###

1. Use the ESSID to iterate though different transmistters.
2. Adjust dinamically the update rate for the ROS publisher taking into account the strength of the receiver.