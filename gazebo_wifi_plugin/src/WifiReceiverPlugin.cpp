#include "WifiReceiverPlugin.hh"

#include "gazebo/sensors/SensorFactory.hh"
#include "gazebo/sensors/SensorManager.hh"
#include "gazebo/sensors/WirelessTransmitter.hh"

#include <ignition/math/Pose3.hh>

using namespace gazebo;
using namespace sensors;
GZ_REGISTER_SENSOR_PLUGIN(WifiReceiverPlugin)

///////////////////////////////
// Wifi Receiver Constructor //
///////////////////////////////
WifiReceiverPlugin::WifiReceiverPlugin() : SensorPlugin()
{
  ROS_INFO_STREAM_NAMED("WifiReceiverPlugin","Loaded");
}

//////////////////////////////
// Wifi Receiver Destructor //
//////////////////////////////
WifiReceiverPlugin::~WifiReceiverPlugin()
{
  this->update_connection_.reset();

  this->sub_queue_.clear();
  this->sub_queue_.disable();
  this->pub_queue_.clear();
  this->pub_queue_.disable();

  this->nh_->shutdown();

  this->pub_callback_queue_thread_.join();
  this->sub_callback_queue_thread_.join();

  delete this->nh_;

  ROS_INFO_STREAM_NAMED("WifiReceiverPlugin","Unloaded");
}

/**
* Function to get the sensor pose in Gazebo world.
* param[in]: sensor, reference to a sensor in world.
* return: Pose3d of sensor in world.
*/
ignition::math::Pose3d WifiReceiverPlugin::GetSensorPose(const sensors::SensorPtr sensor)
{
  boost::weak_ptr<physics::Link> parent =
      boost::dynamic_pointer_cast<physics::Link>(
      this->world_->EntityByName(sensor->ParentName()));
  return sensor->Pose() + parent.lock()->WorldPose();
}

/**
* Inherited function to load sensor plugin in world.
*/
void WifiReceiverPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  // Read sdf paramters
  if (!_sdf->HasElement("update_rate"))
  {
    ROS_WARN("Wifi receiver plugin missing <update_rate>, defaults to 20.0");
    update_rate_ = 20.0;
  }
  else
  {
    update_rate_ = _sdf->GetElement("update_rate")->Get<double>();
  }

  if (!_sdf->HasElement("essid"))
  {
    ROS_WARN("Wifi receiver plugin missing <essid>, defaults to test_wireless");
    router_essid_ = "test_wireless";
  }
  else
  {
    router_essid_ = _sdf->GetElement("essid")->Get<std::string>();
  }

  robot_namespace_ = "/";
  if (_sdf->HasElement("robotNamespace"))
  {
    robot_namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
  }

  if (!_sdf->HasElement("frameName"))
  {
    ROS_WARN("Wifi receiver plugin missing <frameName>, defaults to /world");
    frame_name_ = "/world";
  }
  else
  {
    frame_name_ = _sdf->GetElement("frameName")->Get<std::string>();
  }

  if (!_sdf->HasElement("incoming_topic"))
  {
    ROS_WARN("Wifi receiver plugin missing <incoming_topic>, defaults to /wireless_incoming");
    incoming_topic_ = "/wireless_incoming";
  }
  else
  {
    incoming_topic_ = _sdf->GetElement("incoming_topic")->Get<std::string>();
  }

  if (!_sdf->HasElement("outgoing_topic"))
  {
    ROS_WARN("Wifi receiver plugin missing <outgoing_topic>, defaults to /wireless_outgoing");
    outgoing_topic_ = "/wireless_outgoing";
  }
  else
  {
    outgoing_topic_ = _sdf->GetElement("outgoing_topic")->Get<std::string>();
  }

  // Get the parent sensor (AKA, wireless reveiver)
  this->parent_sensor_ = 
    std::dynamic_pointer_cast<sensors::WirelessReceiver>(_sensor);

  // Make sure the parent sensor is valid.
  if (!this->parent_sensor_)
  {
    gzerr << "WifiReceiverPlugin requires a Wireless Transmitter Sensor.\n";
    return;
  }

  // Get sensor world.
  this->world_ = physics::get_world(this->parent_sensor_->WorldName());

  // Make sure the ROS node for Gazebo has already been initialized
  if (!ros::isInitialized())
  {
    ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load Wireless plugin. "
      << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
    return;
  }

  // Create node handle for ROS communication
  this->nh_ = new ros::NodeHandle(robot_namespace_);

  // Resolve tf prefix
  std::string prefix;
  nh_->getParam(std::string("tf_prefix"), prefix);
  if (robot_namespace_ != "/") {
    prefix = robot_namespace_;
  }
  boost::trim_right_if(prefix, boost::is_any_of("/"));
  frame_name_ = tf::resolve(prefix, frame_name_);

  /*std::cout << "===== Set up Data ====="  << std::endl
  << "robotNamespace: " << robot_namespace_ << std::endl
  << "frameName: "      << frame_name_      << std::endl
  << "incoming_topic: " << incoming_topic_ << std::endl
  << "outgoing_topic: " << outgoing_topic_ << std::endl;*/

  // Advertise publisher with a custom callback queue
  if (outgoing_topic_ != "")
  {
    // Custom Callback Queue
    ros::AdvertiseOptions ao = ros::AdvertiseOptions::create<std_msgs::Float32>(
    this->outgoing_topic_,1,
    boost::bind(&WifiReceiverPlugin::WifiConnect,this),
    boost::bind(&WifiReceiverPlugin::WifiDisconnect,this), ros::VoidPtr(), &this->pub_queue_);
    this->pub_ = this->nh_->advertise(ao);
  }

  this->pub_callback_queue_thread_ = boost::thread(
    boost::bind(&WifiReceiverPlugin::PubQueueThread,this));

  // Sucribe with a custom callback queue
  if (incoming_topic_ != "")
  {
    ros::SubscribeOptions so = ros::SubscribeOptions::create<std_msgs::Float32>(
    this->incoming_topic_,1,
    boost::bind(&WifiReceiverPlugin::OnRosMsg,this,_1),
    ros::VoidPtr(), &this->sub_queue_);
    this->sub_ = this->nh_->subscribe(so);
  }

  this->sub_callback_queue_thread_ = boost::thread(
    boost::bind(&WifiReceiverPlugin::SubQueueThread,this));

  // Connect to the sensor update event.
  this->update_connection_ = this->parent_sensor_->ConnectUpdated(boost::bind(&WifiReceiverPlugin::UpdateImpl, this));

  // Make sure the parent sensor is active.
  this->parent_sensor_->SetActive(true);

  // Set the publish rate of outgoing msgs.
  update_period_ = 1.0 / this->update_rate_;
  last_update_time_ = this->world_->SimTime();
}

/**
* Callback function to read incoming msgs.
* param[in]: _msg, incoming ROS msgs.
*/
void WifiReceiverPlugin::OnRosMsg(const std_msgs::Float32::ConstPtr& _msg)
{
  this->lock_.lock();
  this->ros_msg_ = *_msg;
  this->lock_.unlock();
}

/**
* Bind function to advertise when a topic is connected to the receiver.
*/
void WifiReceiverPlugin::WifiConnect()
{
  std::cout <<
  "Connected"
  << std::endl;
}

/**
* Bind function to advertise when a topic is disconnected to the receiver.
*/
void WifiReceiverPlugin::WifiDisconnect()
{
  std::cout <<
  "Disconnected"
  << std::endl;
}

/**
* Inherited function to implement the communication with the device.
*/
bool WifiReceiverPlugin::UpdateImpl()
{
  const ignition::math::Pose3d this_pose = GetSensorPose(this->parent_sensor_);
  Sensor_V sensors = SensorManager::Instance()->GetSensors();

  for(Sensor_V::iterator it = sensors.begin(); it != sensors.end(); ++it)
  {
    if ((*it)->Type() == "wireless_transmitter")
    {
      sensors::WirelessTransmitterPtr transmit_sensor =
          std::dynamic_pointer_cast<sensors::WirelessTransmitter>(*it);

      const double signal_strength = transmit_sensor->SignalStrength(
          this_pose, this->parent_sensor_->Gain());
      const double tx_freq = transmit_sensor->Freq();
      const std::string tx_essid = transmit_sensor->ESSID();

      /*std::cout << "===== Transmitter Data ====="              << std::endl
      << "Transmitter Essid: "     << tx_essid                 << std::endl
      << "Transmitter Frequency: " << tx_freq                  << std::endl
      << "Transmitter Gain: "      << transmit_sensor->Gain()  << std::endl
      << "Transmitter Power: "     << transmit_sensor->Power() << std::endl;*/

      const ignition::math::Pose3d rel_pose =
          GetSensorPose(transmit_sensor) - this_pose;

      // Check ESSID of incoming msgs
      if(this->router_essid_.compare(tx_essid) != 0)
      {
        continue;
      }

      /*std::cout
      << "===== Pose Data =====" << std::endl
      << "Pose X: "    <<  rel_pose.Pos().X()     << std::endl
      << "Pose Y: "    <<  rel_pose.Pos().Y()     << std::endl
      << "Pose Z: "    <<  rel_pose.Pos().Z()     << std::endl
      << "Rot Yaw: "   <<  rel_pose.Rot().Yaw()   << std::endl
      << "Rot Pitch: " <<  rel_pose.Rot().Pitch() << std::endl
      << "Rot Roll: "  <<  rel_pose.Rot().Roll()  << std::endl;*/

      // Discard if the frequency received is out of our frequency range,
      // or if the received signal strengh is lower than the sensivity
      if ((tx_freq < this->parent_sensor_->MinFreqFiltered())
          || (tx_freq > this->parent_sensor_->MaxFreqFiltered())
          || (signal_strength < this->parent_sensor_->Sensitivity()))
      {
        std::cout << "Signal os sensor:" << transmit_sensor->Name() << " is too weak to publish...: " 
        << "Signal strength: " << signal_strength << std::endl;
        continue;
      }
      else
      {
        // TODO: Change update_period depending on the Signal strength.

        std::cout
        << "===== Router " << transmit_sensor->Name() << " Data =====" << std::endl
        << "Signal strength: " << signal_strength << std::endl;

        common::Time current_time = this->world_->SimTime();
        double seconds_since_last_update = (current_time - last_update_time_).Double();

        if (seconds_since_last_update > update_period_)
        {
          this->lock_.lock();
          this->pub_.publish(this->ros_msg_);
          this->lock_.unlock();
          last_update_time_+= common::Time (update_period_);
        }
      }
    }
  }
  return true;      
}

////////////////////////////////////////////////////////
//////////////// CUSTOM CALLBACK QUEUES ////////////////
////////////////////////////////////////////////////////
void WifiReceiverPlugin::PubQueueThread()
{
  static const double timeout = 0.01;

  while (this->nh_->ok())
  {
    this->pub_queue_.callAvailable(ros::WallDuration(timeout));
  }
}

void WifiReceiverPlugin::SubQueueThread()
{
  static const double timeout = 0.01;

  while (this->nh_->ok())
  {
    this->sub_queue_.callAvailable(ros::WallDuration(timeout));
  }
}