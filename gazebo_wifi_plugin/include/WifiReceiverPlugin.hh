#ifndef _GAZEBO_WIFI_RECEIVER_PLUGIN_HH_
#define _GAZEBO_WIFI_RECEIVER_PLUGIN_HH_

#include <string>

#include <gazebo/gazebo.hh>
#include <gazebo/common/common.hh>
#include <gazebo/sensors/sensors.hh>

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>
#include <ros/subscribe_options.h>
#include <tf/tf.h>

#include <std_msgs/Float32.h>

#include <boost/thread.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>

namespace gazebo
{
  class WifiReceiverPlugin : public SensorPlugin
  {
    public: 
        WifiReceiverPlugin();
        virtual ~WifiReceiverPlugin();
        virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);
        virtual bool UpdateImpl();
        ignition::math::Pose3d GetSensorPose(const sensors::SensorPtr sensor);
    private:
        sensors::WirelessReceiverPtr parent_sensor_;
        event::ConnectionPtr update_connection_;

        std::string router_essid_;

        ros::NodeHandle* nh_;
        ros::Publisher pub_;
        ros::Subscriber sub_;

        std::string topic_name_,
        frame_name_,
        robot_namespace_,
        incoming_topic_,
        outgoing_topic_;

        physics::WorldPtr world_;

        common::Time last_update_time_;
        double update_rate_;
        double update_period_;

        ros::CallbackQueue pub_queue_;
        void PubQueueThread();
        boost::thread pub_callback_queue_thread_;

        ros::CallbackQueue sub_queue_;
        void SubQueueThread();
        boost::thread sub_callback_queue_thread_;
        void OnRosMsg(const std_msgs::Float32::ConstPtr& _msg);
        std_msgs::Float32 ros_msg_;

        boost::mutex lock_;

        void WifiConnect();
        void WifiDisconnect();
  };
}
#endif