# README #

Gazebo Odometry plugin README.

### Description ###

Gazebo Plugin to provide odometry from a world model.

### Set up ###

1. Clone this repository into your catking workspace src folder:
> git clone git@bitbucket.org:weasfas/gazebo_odometry_plugin.git
2. Compile from src:
> catkin_make
3. Launch Gazebo with the Plugin with the all ready made World:
> roslaunch gazebo_odometry_plugin wifi_plugin.launch 

### Param List ###

- robot_namespace (Default: robot_ns)
- update_rate (Default: 20)
- publish_transform (Default: true)
- odometry_topic (Default: odom)
- odometry_frame (Default: odom)
- robot_base_frame (Default: base_footprint)
- rosDebugLevel (Default: na)
- gaussian_noise (Default: 0.0)