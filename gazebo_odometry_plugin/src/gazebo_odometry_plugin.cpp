#include <gazebo_odometry_plugin.hpp>

namespace gazebo
{

  //Construct
  OdomPlugin::OdomPlugin() {}

  // Destructor
  OdomPlugin::~OdomPlugin() {}

  // Plugin loader to Gazebo enviroment
  void OdomPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
  {
      // Store the pointer to the model
      this->model = _parent;

      // Generate Gazebo handle
      gazebo_ros_ = GazeboRosPtr(new GazeboRos(_parent, _sdf, "GazeboOdom"));
      // Init node for Gazebo-Ros
      gazebo_ros_->isInitialized();

      // Parse plugin params from urdf
      //gazebo_ros_->getParameter<std::string> (robot_namespace_, "robot_namespace", "robot_ns");
      gazebo_ros_->getParameter<std::string> (odometry_topic_, "odometry_topic", "odom");
      gazebo_ros_->getParameter<std::string> (odometry_frame_, "odometry_frame", "odom");
      gazebo_ros_->getParameter<std::string> (robot_base_frame_, "robot_base_frame", "base_footprint");
      gazebo_ros_->getParameter<double> (update_rate_, "update_rate", 20.0);
      gazebo_ros_->getParameter<bool> (publish_transform_, "publish_transform", true);
      gazebo_ros_->getParameter<double> (gaussian_noise_, "gaussian_noise", 0.0);

      seed_ = 0.0;

      // Initialize update rate stuff
      if (this->update_rate_ > 0.0) this->update_period_ = 1.0 / this->update_rate_;
      else this->update_period_ = 0.0;
      last_update_time_ = _parent->GetWorld()->SimTime();

      // Generate the TF broadcaster for odometry
      this->transform_broadcaster_ = boost::shared_ptr<tf::TransformBroadcaster>(new tf::TransformBroadcaster());

      // Create the publisher topic
      this->odometry_pub_ = gazebo_ros_->node()->advertise<nav_msgs::Odometry>(odometry_topic_, 1);

      // Listen to the update event (broadcast every simulation iteration)
      this->update_connection_ =
         event::Events::ConnectWorldUpdateBegin(boost::bind(&OdomPlugin::UpdateChild, this ));
}

double OdomPlugin::GuassianKernel(double mu, double sigma)
{
  // generation of two normalized uniform random variables
  double U1 = static_cast<double>(rand_r(&seed_)) / static_cast<double>(RAND_MAX);
  double U2 = static_cast<double>(rand_r(&seed_)) / static_cast<double>(RAND_MAX);

  // using Box-Muller transform to obtain a varaible with a standard normal distribution
  double Z0 = sqrt(-2.0 * ::log(U1)) * cos(2.0*M_PI * U2);

  // scaling
  Z0 = sigma * Z0 + mu;
  return Z0;
}

// Update the controller
void OdomPlugin::UpdateChild()
{

  common::Time current_time = model->GetWorld()->SimTime();
  double seconds_since_last_update = (current_time - last_update_time_).Double();

  if(seconds_since_last_update > update_period_)
  {
    publishOdometry(seconds_since_last_update);

    last_update_time_+= common::Time(update_period_);
  }
}

// Publish model odometry
void OdomPlugin::publishOdometry(double step_time)
{
    ros::Time current_time = ros::Time::now();
    std::string odom_frame = /*gazebo_ros_->resolveTF (*/odometry_frame_/*)*/;
    std::string base_footprint_frame = /*gazebo_ros_->resolveTF (*/robot_base_frame_/*)*/;

    tf::Quaternion qt;
    tf::Vector3 vt;

    // Getting data form gazebo world
    ignition::math::Pose3d pose = model->WorldPose();
    qt = tf::Quaternion ( pose.Rot().X(), pose.Rot().Y(), pose.Rot().Z(), pose.Rot().W() );
    vt = tf::Vector3 ( pose.Pos().X(), pose.Pos().Y(), pose.Pos().Z() );

    odom_.pose.pose.position.x = vt.x() + GuassianKernel(0,gaussian_noise_);
    odom_.pose.pose.position.y = vt.y() + GuassianKernel(0,gaussian_noise_);
    odom_.pose.pose.position.z = vt.z() + GuassianKernel(0,gaussian_noise_);

    odom_.pose.pose.orientation.x = qt.x() + GuassianKernel(0,gaussian_noise_);
    odom_.pose.pose.orientation.y = qt.y() + GuassianKernel(0,gaussian_noise_);
    odom_.pose.pose.orientation.z = qt.z() + GuassianKernel(0,gaussian_noise_);
    odom_.pose.pose.orientation.w = qt.w() + GuassianKernel(0,gaussian_noise_);

    // Get velocity in /odom frame
    ignition::math::Vector3d linear;
    linear = model->WorldLinearVel();
    odom_.twist.twist.angular.z = model->WorldAngularVel().Z() + GuassianKernel(0,gaussian_noise_);

    // convert velocity to child_frame_id (aka base_footprint)
    float yaw = pose.Rot().Yaw();
    odom_.twist.twist.linear.x = (cosf (yaw) * linear.X() + sinf (yaw) * linear.Y()) + GuassianKernel(0,gaussian_noise_);
    odom_.twist.twist.linear.y = (cosf (yaw) * linear.Y() - sinf (yaw) * linear.X()) + GuassianKernel(0,gaussian_noise_);

    tf::Transform base_footprint_to_odom ( qt, vt );
    
    if(publish_transform_)
    {
      transform_broadcaster_->sendTransform (
        tf::StampedTransform ( base_footprint_to_odom, current_time,
                               odom_frame, base_footprint_frame ) );
    }

    double gaussian_noise_2 = gaussian_noise_ * gaussian_noise_;
    // set covariance
    odom_.pose.covariance[0] = gaussian_noise_2;
    odom_.pose.covariance[7] = gaussian_noise_2;
    odom_.pose.covariance[14] = gaussian_noise_2;
    odom_.pose.covariance[21] = gaussian_noise_2;
    odom_.pose.covariance[28] = gaussian_noise_2;
    odom_.pose.covariance[35] = gaussian_noise_2;


    // set header
    odom_.header.stamp = current_time;
    odom_.header.frame_id = odom_frame;
    odom_.child_frame_id = base_footprint_frame;

    odometry_pub_.publish(odom_);
}

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(OdomPlugin)
}
