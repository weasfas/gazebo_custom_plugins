#ifndef GAZEBO_ODOM_PLUGIN_HH
#define GAZEBO_ODOM_PLUGIN_HH

// Gazebo
#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <ignition/math/Pose3.hh>
#include <gazebo_plugins/gazebo_ros_utils.h>

// ROS
#include "ros/ros.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"
#include "nav_msgs/Odometry.h"

namespace gazebo {

class OdomPlugin : public ModelPlugin
{
    public:
        OdomPlugin();
        ~OdomPlugin();
        void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    protected:
        virtual void UpdateChild();
        double GuassianKernel(double mu, double sigma);

    private:
        void publishOdometry(double step_time);

        //////////////// GAZEBO VARIABLES ////////////////
        // Pointer to the model
        physics::ModelPtr model;
        // Gazebo handler
        GazeboRosPtr gazebo_ros_;

        // Update event from Gazebo
        event::ConnectionPtr update_connection_;

        //////////////// CONTROL VARIABLES ////////////////
        double update_rate_;
        double update_period_;
        common::Time last_update_time_;
        bool publish_transform_;

        double gaussian_noise_;
        unsigned int seed_;

        ////////////////// URDF VARIABLES //////////////////
        // std::string robot_namespace_; // Not needed because it is supposed to run with ns tag.
        std::string odometry_topic_;
        std::string odometry_frame_;
        std::string robot_base_frame_;

        //////////////// ROS VARIABLES ////////////////
        // ROS publisher 
        ros::Publisher odometry_pub_;
        // TF broadcaster form odom
        boost::shared_ptr<tf::TransformBroadcaster> transform_broadcaster_;
        // Odom msg
        nav_msgs::Odometry odom_;
  };
}

#endif

