#include "gazebo_mouse_track.hpp"

using namespace gazebo;

SystemGUI::~SystemGUI()
{
    this->connections_.clear();
    if (this->user_cam_)
    {
        this->user_cam_->EnableSaveFrame(false);
    }
    this->user_cam_.reset();
}

void SystemGUI::Load(int /*_argc*/, char ** /*_argv*/)
{
    this->connections_.push_back(
        event::Events::ConnectPreRender(boost::bind(&SystemGUI::Update, this)));

    gui::MouseEventHandler::Instance()->AddPressFilter("glwidget", boost::bind(&SystemGUI::OnMousePress, this, _1));
}

void SystemGUI::Init()
{
    gzmsg << "Gazebo Mouse track plugin loaded properly!"<< std::endl;
}

bool SystemGUI::OnMousePress(const common::MouseEvent& _event)
{
    boost::mutex::scoped_lock lock(mutex_mouse_clicked_);
    mouse_clicked_ = _event.Pos();
    is_mouse_clicked_ = true;
    gzmsg << "Mouse Track Plugin: Clicked at " << mouse_clicked_ <<" of screen."<< std::endl;
}

void SystemGUI::Update()
{
    if (!this->user_cam_)
    {
        // Get a pointer to the active user camera
        this->user_cam_ = gui::get_active_camera();
    }

    // Get scene pointer
    rendering::ScenePtr scene = rendering::get_scene();

    // Wait until the scene is initialized.
    if (!scene || !scene->Initialized())
        return;

    if(is_mouse_clicked_ && this->user_cam_)
    {
        boost::mutex::scoped_lock lock(mutex_mouse_clicked_);
        ignition::math::Vector3d position_clicked;
        scene->FirstContact(this->user_cam_, mouse_clicked_, position_clicked);
        gzmsg << "Mouse Track Plugin: Clicked at " << position_clicked << " of world." << std::endl;
        is_mouse_clicked_ = false;
    }
}

// Register this plugin with the simulator
GZ_REGISTER_SYSTEM_PLUGIN(SystemGUI)
