# README #

Gazebo Mouse plugin README.

### Description ###

Gazebo Plugin to implement a mouse tracking service. It will allow to print mouse click coordinates either camera and world.

### Set up ###

1. Clone this repository into your catking workspace src folder:
> git clone git@bitbucket.org:weasfas/gazebo_wifi_plugin.git
2. Compile from src:
> catkin_make
3. Launch Gazebo with the Plugin with the all ready made World:
> roslaunch gazebo_mouse_plugin coordinates_plugin.launch