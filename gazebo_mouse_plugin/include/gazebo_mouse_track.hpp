#ifndef _GAZEBO_MOUSE_TRACK_PLUGIN_HH_
#define _GAZEBO_MOUSE_TRACK_PLUGIN_HH_

#include <ignition/math/Rand.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/gui/MouseEventHandler.hh>
#include <gazebo/common/MouseEvent.hh>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

namespace gazebo
{
	class SystemGUI : public SystemPlugin
	{
		public: 
			virtual ~SystemGUI();
			void Load(int /*_argc*/, char ** /*_argv*/);

		private:
			void Init();
			bool OnMousePress(const common::MouseEvent& _event);
			void Update();

			boost::mutex mutex_mouse_clicked_;
			ignition::math::Vector2i mouse_clicked_;
			bool is_mouse_clicked_;

			rendering::UserCameraPtr user_cam_;
			std::vector<event::ConnectionPtr> connections_;
	};
}
#endif